# Copyright 2017-2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'attrs-16.3.0-r1.ebuild' from Gentoo, which is:
#   Copyright 1999-2017 Gentoo Foundation

require pypi
require setup-py [ import=setuptools python_opts=[sqlite] ]

SUMMARY="Attributes without boilerplate"

LICENCES="MIT"

SLOT="0"

PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"

MYOPTIONS=""
DEPENDENCIES="
    run+test:
        dev-python/zopeinterface[python_abis:*(-)?]
    test:
        dev-python/coverage[python_abis:*(-)?]
        dev-python/Pympler[python_abis:*(-)?]
        dev-python/six[python_abis:*(-)?]
        dev-python/zopeinterface[python_abis:*(-)?]
    post:
        dev-python/hypothesis[>=3.6.0][python_abis:*(-)?]
        dev-python/pytest[python_abis:*(-)?]
"

_test_dependencies_satisfied() {
    has_version "dev-python/hypothesis[>=3.6.0][python_abis:$(python_get_abi)]" || return 1
    has_version "dev-python/pytest[python_abis:$(python_get_abi)]" || return 1
}

test_one_multibuild() {
    # avoid a attrs <-> hypothesis <--> pytest dependency loop
    if _test_dependencies_satisfied; then
        # From setup-py.exlib, can't use setup-py_test_one_multibuild, that
        # adds a dependency to dev-python/pytest itself
        PYTEST="py.test-$(python_get_abi)"

        PYTHONPATH="$(ls -d ${PWD}/build/lib*)" edo ${PYTEST} "${PYTEST_PARAMS[@]}"
    else
        ewarn "dev-python/hypothesis not yet installed, skipping tests"
    fi
}

