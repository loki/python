# Copyright 2014 Calvin Walton
# Distributed under the terms of the GNU General Public License v2

MY_PN=${PN/-/.}
MY_PNV=${MY_PN}-${PV}
require pypi [ pn=${MY_PN} pnv=${MY_PNV} ]
# Backport from python 3.5
require setup-py [ import=distutils work=${MY_PNV} blacklist='3.5 3.6 3.7' ]

SUMMARY="The ssl.match_hostname() function from Python 3.5"
DESCRIPTION="
The Secure Sockets layer is only actually secure if you check the
hostname in the certificate returned by the server to which you are
connecting, and verify that it matches to hostname that you are trying
to reach.

But the matching logic, defined in RFC2818, can be a bit tricky to
implement on your own. So the ssl package in the Standard Library of
Python 3.2 and greater now includes a match_hostname() function for
performing this check instead of requiring every application to
implement the check separately.

This backport brings match_hostname() to users of earlier versions of
Python.
"
HOMEPAGE="http://bitbucket.org/brandon/backports.ssl_match_hostname"

LICENCES="PSF"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-python/backports[python_abis:*(-)?]
"

install_one_multibuild() {
    setup-py_install_one_multibuild

    # Remove files provided by umbrella namespace package (dev-python/backports)
    [[ $(python_get_abi) == 2.* ]] && edo rm \
        "${IMAGE}"/$(python_get_sitedir)/backports/__init__.py{,c,o}

    [[ $(python_get_abi) == 3.* ]] && edo rm -r \
        "${IMAGE}"/$(python_get_sitedir)/backports/{__init__.py,__pycache__}
}


